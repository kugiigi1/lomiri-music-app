#!/usr/bin/python3

import os
import sys
from pathlib import Path

organization = "music.ubports"
application = "music.ubports"
old_application = "com.ubuntu.music"

xdg_config_home = Path(os.environ.get("XDG_CONFIG_HOME",
                                      Path.home() / ".config"))
old_config_file = xdg_config_home / organization / f"{old_application}.conf"
new_config_file = xdg_config_home / organization / f"{application}.conf"
if old_config_file.is_file() and not new_config_file.exists():
    with old_config_file.open() as old, \
         new_config_file.open(mode="w+") as new:
        for line in old:
            if line.strip() == f"repeat=true":
                line = f"repeat=repeatAlbum\n"
            new.write(line)
    old_config_file.unlink()

if len(sys.argv) > 1:
    os.execvp(sys.argv[1], sys.argv[1:])
